import { schema } from 'normalizr';

import user from 'state/users/schema';

const auth = new schema.Entity('auth');
auth.define({
  fires: {
    notifications: {
      matchedFires: [
        { to: user },
      ],
      superFires: [
        { from: user },
      ],
    },
  },
});

export const relationships = new schema.Entity('relationships', {
  user,
  relatesTo: user,
});

export const fires = new schema.Entity('fires', {
  notifications: {
    matchedFires: [
      { to: user },
    ],
    superFires: [
      { from: user },
    ],
  },
});

export default auth;
