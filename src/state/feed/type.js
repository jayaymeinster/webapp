import PropTypes from 'prop-types';

export default PropTypes.shape({
  id: PropTypes.string.isRequired,
  author: PropTypes.number,
  createdAt: PropTypes.string.isRequired,
  reactedByUserIds: PropTypes.arrayOf(PropTypes.number).isRequired,
});
