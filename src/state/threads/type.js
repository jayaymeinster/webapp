import PropTypes from 'prop-types';

export default PropTypes.shape({
  title: PropTypes.string.isRequired,
  createdAt: PropTypes.string.isRequired,
  author: PropTypes.number.isRequired,
  replyCount: PropTypes.number.isRequired,
});
