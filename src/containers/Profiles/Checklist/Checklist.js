import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';

import Api from 'state/api';
import { useTranslation } from 'hooks';
import * as userSelectors from 'state/users/selectors';

import Spinner from 'components/Spinner';
import SpinnerWrapper from 'components/Spinner/Wrapper';
import EmptyState from 'components/EmptyState';

import ChecklistWrapper from './Wrapper';
import NoAccess from './NoAccess';
import locales from './i18n';

const Checklist = () => {
  const { t } = useTranslation(locales);
  const params = useParams();

  const userId = useSelector(userSelectors.getByUsername(params.username));
  const pronoun = useSelector(userSelectors.getPronoun(userId));

  const [checklist, setChecklist] = useState(null);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetch = async () => {
      try {
        const { data } = await Api.req.get(`/users/${params.username}/checklist`);
        setChecklist(data);
      } catch (e) {
        setError(e.response.status);
      }
    };

    fetch();
  }, [params.username]);

  if (error === 403) return <NoAccess />;
  if (error) return <ChecklistWrapper><EmptyState title={t('You can\'t access this checklist at this moment')} subtitle={t('Try again later')} /></ChecklistWrapper>;
  if (checklist === null) return <SpinnerWrapper><Spinner color="#999" /></SpinnerWrapper>;

  const allItemsCount = checklist.reduce((acc, current) => (
    acc + current.selections.filter(i => !!i.item).length
  ), 0);
  if (allItemsCount === 0) return <EmptyState subtitle={t('This checklist is empty')} />;

  return (
    <ChecklistWrapper>
      {checklist.map((section) => {
        const selections = section.selections.filter(i => !!i.item);

        if (!selections.length) return null;
        return (
          <div key={`checklist-category-${section.category}`}>
            <h3>{t(section.category)}</h3>
            <table>
              <thead>
                <tr>
                  <th />
                  <th>{t('Experience')}</th>
                  <th>{t('Interest')}</th>
                </tr>
              </thead>
              <tbody>
                {selections.map(selection => (
                  <tr key={`checklist-selection-${selection.item.id}`}>
                    <td>{t(selection.item.name, { context: pronoun })}</td>
                    <td>{t(`EXPERIENCE.${selection.experience}`)}</td>
                    <td>{t(`INTEREST.${selection.interest}`)}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        );
      })}
    </ChecklistWrapper>
  );
};

Checklist.propTypes = {
};

Checklist.defaultProps = {
};

export default Checklist;
