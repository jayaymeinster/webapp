import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useSelector, shallowEqual } from 'react-redux';
import { HStack, Text } from '@chakra-ui/react';

import * as userSelectors from 'state/users/selectors';

import UserCountryFlag from 'components/UserCountryFlag';

const LocationContainer = styled.div`
  font-size: 16px;
  color: #a7a7a7;
  margin: 4px 0 16px;
  text-align: center;

  img {
    height: 16px;
    margin-right: 8px;
  }
`;
LocationContainer.displayName = 'LocationContainer';

const Location = ({ userId }) => {
  const user = useSelector(userSelectors.getById(userId), shallowEqual);
  const isOrganization = useSelector(userSelectors.isOrganization(userId));

  if (isOrganization) return null;

  const location = [];
  if (user.city) location.push(user.city.name);
  if (user.region) location.push(user.region.name);
  if (user.country) location.push(user.country.name);

  if (!user.country) return null;

  return (
    <HStack mb={4}>
      <UserCountryFlag userId={userId} h={4} />
      <Text color="gray.300">{location.length > 0 && location.join(', ')}</Text>
    </HStack>
  );
};

Location.propTypes = {
  userId: PropTypes.number.isRequired,
};

Location.defaultProps = {
};

export default Location;
