import { useCallback } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as userSelectors from 'state/users/selectors';
import * as userActions from 'state/users/actions';

import Modal from 'components/Modal';
import UserList from 'components/UserList';

import locales from '../../i18n';

const Followers = ({ userId, close }) => {
  const { t } = useTranslation(locales);

  const pronoun = useSelector(userSelectors.getPronoun(userId));

  const fetchAction = useCallback(() => userActions.getFollowers(userId), [userId]);

  return (
    <Modal
      fullHeight
      title={t('Followed by', { context: pronoun })}
      onClose={close}
    >
      <UserList fetchAction={fetchAction} onUserClick={close} />
    </Modal>
  );
};

Followers.propTypes = {
  userId: PropTypes.number.isRequired,
  close: PropTypes.func.isRequired,
};

Followers.defaultProps = {
};

export default Followers;
