import { useCallback } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as userActions from 'state/users/actions';
import * as userSelectors from 'state/users/selectors';

import Modal from 'components/Modal';
import UserList from 'components/UserList';

import locales from '../../i18n';

const KnownBy = ({ userId, close }) => {
  const { t } = useTranslation(locales);
  const pronoun = useSelector(userSelectors.getPronoun(userId));

  const fetchAction = useCallback(() => userActions.getKnowing(userId), [userId]);

  return (
    <Modal
      fullHeight
      title={t('Known by', { context: pronoun })}
      onClose={close}
    >
      <UserList fetchAction={fetchAction} onUserClick={close} />
    </Modal>
  );
};

KnownBy.propTypes = {
  userId: PropTypes.number.isRequired,
  close: PropTypes.func.isRequired,
};

KnownBy.defaultProps = {
};

export default KnownBy;
