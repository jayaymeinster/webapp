import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { HStack, Text } from '@chakra-ui/react';

import { useTranslation } from 'hooks';
import * as userSelectors from 'state/users/selectors';

import { GenderTransgender, CakeVariant } from 'components/Icons';

import locales from '../i18n';

const GenderAge = ({ userId }) => {
  const { t } = useTranslation(locales);

  const isOrganization = useSelector(userSelectors.isOrganization(userId));
  const gender = useSelector(userSelectors.getGender(userId));
  const age = useSelector(userSelectors.getAge(userId));

  if (isOrganization) return null;

  return (
    <HStack justifyContent="center" spacing={4}>
      {gender && (
        <HStack justifyContent="center" spacing={1}>
          <GenderTransgender fill="gray.400" boxSize={4} />
          <Text>{t(`global:GENDER.${gender}`)}</Text>
        </HStack>
      )}
      <HStack justifyContent="center" spacing={1}>
        <CakeVariant fill="gray.400" boxSize={4} />
        <Text>{t('{{age}} years old', { age })}</Text>
      </HStack>
    </HStack>
  );
};

GenderAge.propTypes = {
  userId: PropTypes.number.isRequired,
};

GenderAge.defaultProps = {
};

export default GenderAge;
