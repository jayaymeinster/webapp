import { useState, useMemo } from 'react';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import { Trans } from 'react-i18next';
import shortid from 'shortid';
import {
  Box, Button, Tag, TagCloseButton, TagLabel, Icon, Text, Wrap,
} from '@chakra-ui/react';

import { useTranslation } from 'hooks';
import useToast from 'hooks/useToast';
import * as authSelectors from 'state/auth/selectors';
import * as authActions from 'state/auth/actions';

import H2 from 'ui/H2';
import DraggableArea from 'components/DraggableArea';
import DiamondStone from 'components/Icons/DiamondStone';

import { USERTAGS } from '../../../../constants';
import locales from '../i18n';
import PurchaseTagModal from './PurchaseTagModal';

const Tags = () => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();
  const toast = useToast();

  const tags = useSelector(authSelectors.selectTags, shallowEqual);
  const purchased = useSelector(authSelectors.tagsPurchased);

  const [adding, setAdding] = useState(null);
  const [removing, setRemoving] = useState(null);
  const [purchase, setPurchase] = useState(null);

  const closeTagPurchase = () => setPurchase(null);

  const add = (tag) => async () => {
    try {
      setAdding(tag);

      await dispatch(authActions.addTag(tag));
      toast.success(t('Tag successfully added'));
    } catch (error) {
      if (error.response?.status === 400 && error.response?.data?.message === 'NEEDS_PURCHASE') {
        setPurchase({ ...error.response.data.data, tag });
      } else {
        toast.error(error);
      }
    }

    setAdding(null);
  };

  const remove = (tag) => async () => {
    try {
      setRemoving(tag);

      await dispatch(authActions.removeTag(tag));
      toast.success(t('Tag removed'));
    } catch (error) {
      toast.error(error);
    }

    setRemoving(null);
  };

  const sortTags = async (newTags) => {
    try {
      await dispatch(authActions.sortTags(newTags.map(tag => tag.content)));
    } catch (error) {
      // Do nothing. Fail quietly.
    }
  };

  const groups = useMemo(() => {
    const res = {};

    Object.keys(USERTAGS).forEach((tagkey) => {
      const tag = USERTAGS[tagkey];
      if (!res[tag.group]) res[tag.group] = [];
      res[tag.group].push({ ...tag, key: tagkey });
    });

    return res;
  }, []);

  const selectedTags = tags
    .filter((tag) => tag !== removing)
    .map(tag => ({ id: tag, content: tag }));

  return (
    <>
      <Box>
        <Trans t={t} i18nKey="tags.explanation" ns="userEdit">
          <Text mb={4}>
            Choose the tags that you feel define your sexuality.
            You can change them as many times as you want.
          </Text>
        </Trans>

        <Box>
          <H2>{t('Select and sort your tags')}</H2>
          {selectedTags.length > 0 && (
            <Wrap>
              <DraggableArea
                mb={2}
                tags={selectedTags}
                render={({ tag }) => (
                  <Tag key={`usertag-general-${tag.content}`} mr={4} size="lg" colorScheme="red">
                    <TagLabel>{t(`global:TAG.${tag.content}`)}</TagLabel>
                    <TagCloseButton onClick={remove(tag.content)} />
                  </Tag>
                )}
                onChange={sortTags}
              />
            </Wrap>
          )}

          <Text fontSize="sm" color="gray.400">{t('Selected tags {{selected}}', { selected: tags.length })}</Text>

          <Box mt={6}>
            {Object.values(groups).map(group => (
              <Box key={`taggroup-${shortid.generate()}`} mb={4}>
                {Object.values(group).filter((tag) => !tags.includes(tag.label)).map((tag) => (
                  <Button
                    key={`usertag-general-${tag.label}`}
                    mr={3}
                    mb={2}
                    opacity={tag.price > 0 && !purchased.includes(tag.label) ? 0.5 : 1}
                    fontWeight="normal"
                    size="xs"
                    onClick={add(tag.key)}
                    leftIcon={tag.price > 0 && <Icon as={DiamondStone} boxSize={3} />}
                    isLoading={tag.label === adding}
                    isDisabled={adding && tag.label !== adding}
                  >
                    {t(`global:TAG.${tag.label}`)}
                  </Button>
                ))}
              </Box>
            ))}
          </Box>
        </Box>
      </Box>

      {/* Modals */}
      {!!purchase && (
        <PurchaseTagModal purchase={purchase} onClose={closeTagPurchase} add={add} />
      )}
    </>
  );
};

Tags.propTypes = {
};

Tags.defaultProps = {
};

export default Tags;
