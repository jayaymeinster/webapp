import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import Api from 'state/api';
import { useTranslation } from 'hooks';

import Modal from 'components/Modal';
import Loading from 'components/Loading';

import Secret from '../Secret';
import locales from '../i18n';

const Wrapper = styled.div`
  text-align: center;
`;
Wrapper.displayName = 'Wrapper';

const SecretModal = ({
  id, onCancel,
}) => {
  const { t } = useTranslation(locales);

  const [secret, setSecret] = useState(null);

  useEffect(() => {
    const renew = async () => {
      const { data } = await Api.req.put(`/chat/bots/${id}/secret`);
      setSecret(data.secret);
    };

    renew();
  }, [id]);

  return (
    <Modal onClose={secret && onCancel}>
      {secret === null
        ? <Loading />
        : (
          <Wrapper>
            <h2>{t('This is the bot\'s secret key')}</h2>
            <Secret>{secret}</Secret>

            <p>{t('secret.explain')}</p>
          </Wrapper>
        )
      }
    </Modal>
  );
};

SecretModal.propTypes = {
  id: PropTypes.string.isRequired,
  onCancel: PropTypes.func.isRequired,
};

SecretModal.defaultProps = {
};

export default SecretModal;
