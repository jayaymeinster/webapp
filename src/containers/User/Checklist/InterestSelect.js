import { useMemo } from 'react';
import PropTypes from 'prop-types';

import { useTranslation } from 'hooks';

import locales from 'containers/Profiles/Checklist/i18n';
import Select from 'components/Select';

const InterestSelect = ({ onChange, selection }) => {
  const { t } = useTranslation(locales);

  const options = useMemo(() => ([
    { value: null, label: '' },
    { value: 'HARD_LIMIT', label: t('INTEREST.HARD_LIMIT') },
    { value: 'SOFT_LIMIT', label: t('INTEREST.SOFT_LIMIT') },
    { value: 'NONE', label: t('INTEREST.NONE') },
    { value: 'LITTLE', label: t('INTEREST.LITTLE') },
    { value: 'SOME', label: t('INTEREST.SOME') },
    { value: 'LOT', label: t('INTEREST.LOT') },
  ]), [t]);

  const value = selection
    ? options.find(o => o.value === selection.interest) : null;

  return (
    <Select options={options} value={value} placeholder="" small onChange={onChange} />
  );
};

InterestSelect.propTypes = {
  onChange: PropTypes.func.isRequired,
  selection: PropTypes.shape({
    interest: PropTypes.string.isRequired,
  }),
};

InterestSelect.defaultProps = {
  selection: null,
};

export default InterestSelect;
