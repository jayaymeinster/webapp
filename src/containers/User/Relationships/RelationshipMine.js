import { useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';

import { useTranslation, useOpenClose } from 'hooks';
import * as authSelectors from 'state/auth/selectors';
import * as userSelectors from 'state/users/selectors';
import * as authActions from 'state/auth/actions';
import * as appActions from 'state/app/actions';

import UserDisplayName from 'components/UserDisplayName';
import UserAvatar from 'components/UserAvatar';
import UserLink from 'components/UserLink';
import { TrashCan } from 'components/Icons';
import Button from 'components/Button';
import Modal from 'components/Modal';

import { RELATIONSHIP_TYPES } from '../../../constants';
import locales from './i18n';

const RelationshipMine = ({
  userId, type, approved, relationshipId,
}) => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();

  const myUserId = useSelector(authSelectors.selectUserId);
  const displayname = useSelector(userSelectors.getDisplayName(userId));

  const [showingRemoveModal, openRemoveModal, closeRemoveModal] = useOpenClose(false);

  const [removing, setRemoving] = useState(false);

  const remove = useCallback(async () => {
    try {
      setRemoving(true);
      await dispatch(authActions.removeRelationship(myUserId, relationshipId));
      dispatch(appActions.addToast(t('Relationship removed')));
    } catch (error) {
      dispatch(appActions.addError(error));
    }

    closeRemoveModal();
  }, [dispatch, t, closeRemoveModal, myUserId, relationshipId]);

  return (
    <tr>
      <td className="description">
        {t(`global:RELATION.${type}`)}
        {' '}
        {t('of')}
        {' '}
        <UserLink userId={userId}>
          <UserAvatar userId={userId} size="16px" showOnline={false} />
          <UserDisplayName userId={userId} />
        </UserLink>
        {!approved && (
          <div className="pending">{t('Pending')}</div>
        )}
      </td>
      <td className="actions">
        <div>
          <Button className="empty" onClick={openRemoveModal}><TrashCan color="#A8A8A8" /></Button>
        </div>
      </td>

      {/* Modals */}
      {showingRemoveModal && (
        <Modal
          title={t('Remove Relationship')}
          onCancel={closeRemoveModal}
          actions={[
            <Button key="relationsip-mine-remove" onClick={remove} loading={removing}>{t('global:Confirm')}</Button>,
          ]}
        >
          {t('Are you sure you want to cancel your relationship with {{name}}?', { name: displayname })}
        </Modal>
      )}
    </tr>
  );
};

RelationshipMine.propTypes = {
  userId: PropTypes.number.isRequired,
  relationshipId: PropTypes.string.isRequired,
  type: PropTypes.oneOf(Object.values(RELATIONSHIP_TYPES)).isRequired,
  approved: PropTypes.bool.isRequired,
};

RelationshipMine.defaultProps = {
};

export default RelationshipMine;
