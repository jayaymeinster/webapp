import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useSelector, shallowEqual } from 'react-redux';

import { useTranslation } from 'hooks';
import * as authSelectors from 'state/auth/selectors';

import Spinner from 'components/Spinner';
import ErrorState from 'components/ErrorState';

import RelationshipOther from './RelationshipOther';
import RelationshipMine from './RelationshipMine';
import AddRelationship from './AddRelationship';
import locales from './i18n';

const SpinnerWrapper = styled.div`
  text-align: center;
  margin: 32px;
`;
SpinnerWrapper.displayName = 'SpinnerWrapper';

const Wrapper = styled.div`
  h2 {
    color: #331A1A;
    font-size: 20px;
    font-weight: 500;
    margin-bottom: 12px;
  }

  table {
    width: 100%;
    margin-bottom: 16px;

    td.description {
      display: flex;
      padding: 8px 0;

      a {
        color: ${props => props.theme.colors.main};
      }

      .userlink {
        display: flex;

        .avatar {
          display: flex;
        }

        .displayname {
          margin-right: 8px;
        }
      }

      .pending {
        color: ${props => props.theme.colors.secondaryText};
        background-color: ${props => props.theme.colors.disabledBackground};
        font-size: 12px;
        padding: 4px;
        line-height: 12px;
        margin-left: 8px;
      }
    }

    &.myRelationships {
      .userlink {
        margin-left: 8px;
      }
    }

    td.actions {
      > div {
        > button > div > div {
          margin: 0;

          svg {
            width: 24px;
            height: 24px;
          }

          &:hover svg path {
            fill: ${props => props.theme.colors.main};
          }
        }
      }
    }
  }
`;
Wrapper.displayName = 'Wrapper';

const RelationshipsContent = ({ hasError }) => {
  const { t } = useTranslation(locales);

  const userId = useSelector(authSelectors.selectUserId);
  const relationships = useSelector(authSelectors.selectRelationships, shallowEqual);

  if (hasError) {
    return (
      <ErrorState
        message={t('An error ocurred trying to fetch your relationships. Please try again in a moment.')}
        reload
      />
    );
  }

  if (typeof relationships === 'undefined') {
    return (
      <SpinnerWrapper>
        <Spinner color="#A8A8A8" />
      </SpinnerWrapper>
    );
  }

  const mine = relationships.filter(relationship => relationship.user === userId);
  const others = relationships.filter(relationship => relationship.user !== userId);

  return (
    <Wrapper>
      <h2>{t('Relationships others have with me')}</h2>
      <table className="otherRelationships">
        <tbody>
          {others.map(relationship => (
            <RelationshipOther
              key={`relationshi-mine-${relationship.user}`}
              userId={relationship.user}
              type={relationship.type}
              relationshipId={relationship.id}
              approved={relationship.approved}
            />
          ))}
        </tbody>
      </table>

      <h2>{t('Relationships I have with others')}</h2>
      <table className="myRelationships">
        <tbody>
          {mine.map(relationship => (
            <RelationshipMine
              key={`relationshi-mine-${relationship.relatesTo}`}
              userId={relationship.relatesTo}
              type={relationship.type}
              relationshipId={relationship.id}
              approved={relationship.approved}
            />
          ))}
        </tbody>
      </table>

      <AddRelationship />
    </Wrapper>
  );
};

RelationshipsContent.propTypes = {
  hasError: PropTypes.bool.isRequired,
};

RelationshipsContent.defaultProps = {
};

export default RelationshipsContent;
