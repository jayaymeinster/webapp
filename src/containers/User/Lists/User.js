import { useCallback, useState, useMemo } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';

import { useOpenClose, useTranslation } from 'hooks';
import * as authSelectors from 'state/auth/selectors';
import * as authActions from 'state/auth/actions';
import * as appActions from 'state/app/actions';

import { SelectableListUser } from 'components/SelectableList';
import UsersListsModal from 'components/UserFollowButton/UsersListsModal';
import Button from 'components/Button';
import { Pencil, TrashCan } from 'components/Icons';

import locales from './i18n';

const User = ({ userId, listId }) => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();

  const inLists = useSelector(state => authSelectors.selectInListsIds(state, userId), shallowEqual);

  const [showingListsModal, openListsModal, closeListsModal] = useOpenClose();
  const [loading, setLoading] = useState(false);

  const saveFromLists = useCallback(async (lists) => {
    try {
      setLoading(true);
      await dispatch(authActions.listsChange(userId, lists));
      dispatch(appActions.addToast(t('Follow updated')));
    } catch (error) {
      dispatch(appActions.addError(error));
    }

    setLoading(false);
    closeListsModal();
  }, [dispatch, t, closeListsModal, userId]);

  const removeFromList = useCallback(async () => {
    try {
      setLoading(true);
      await dispatch(authActions.removeFromList(userId, listId));
      dispatch(appActions.addToast(t('User removed from Follow List')));
    } catch (error) {
      dispatch(appActions.addError(error));
    }

    setLoading(false);
  }, [dispatch, t, userId, listId]);

  const openMiniProfile = useCallback((evt) => {
    if (!evt.target.closest('button')) {
      dispatch(appActions.setMiniprofileByUserId(userId));
    }
  }, [dispatch, userId]);

  const actions = useMemo(() => {
    if (loading) return [<Button key={`userlist-${userId}-loading`} className="empty" loading />];
    return [
      <Button key={`userlist-${userId}-edit`} className="empty" onClick={openListsModal}><Pencil color="#A8A8A8" /></Button>,
      <Button key={`userlist-${userId}-remove`} className="empty" onClick={removeFromList}><TrashCan color="#A8A8A8" /></Button>,
    ];
  }, [loading, userId, openListsModal, removeFromList]);

  return (
    <>
      <SelectableListUser
        userId={userId}
        actions={actions}
        onClick={openMiniProfile}
      />

      {showingListsModal && (
        <UsersListsModal
          close={closeListsModal}
          initialLists={inLists}
          onConfirm={saveFromLists}
          confirmLoading={loading}
        />
      )}
    </>
  );
};

User.propTypes = {
  userId: PropTypes.number.isRequired,
  listId: PropTypes.string.isRequired,
};

User.defaultProps = {
};

export default User;
