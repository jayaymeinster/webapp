import styled from 'styled-components';
import { useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';

import * as authSelectors from 'state/auth/selectors';

import NotFound from 'containers/NotFound';
import Sidebar from 'containers/Sidebar';
import Layout from 'components/Layout';
import { FlexWrapper, FlexInnerWrapper, FlexContainer } from 'components/FlexWrapper';

import ListName from './ListName';
import ListUsers from './ListUsers';
import ListActions from './ListActions';

const Wrapper = styled.div`
  .empty svg {
    width: 24px;
    height: 24px;

    &:hover path {
      fill: ${props => props.theme.colors.main};
    }
  }
`;
Wrapper.displayName = 'Wrapper';

const List = () => {
  const { listId } = useParams();

  const listExists = useSelector(state => authSelectors.selectExistsFollowList(state, listId));

  if (!listExists) return <NotFound />;

  return (
    <Layout columns={2} feed leftColumnOpen={false} rightColumnOpen={false}>
      <FlexWrapper canOverflow>
        <FlexInnerWrapper>
          <ListName listId={listId} />

          <FlexContainer framed>
            <ListActions listId={listId} />

            <Wrapper>
              <ListUsers listId={listId} />
            </Wrapper>
          </FlexContainer>
        </FlexInnerWrapper>
      </FlexWrapper>

      <Sidebar />
    </Layout>
  );
};

List.propTypes = {
};

List.defaultProps = {
};

export default List;
