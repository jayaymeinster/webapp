import { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import { useTranslation } from 'hooks';
import * as authActions from 'state/auth/actions';

import Loading from 'components/Loading';
import EmptyState from 'components/EmptyState';
import Button from 'components/Button';

import locales from './i18n';

const Confirm = () => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();
  const params = useParams();

  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    const load = async () => {
      try {
        await dispatch(authActions.confirmEmail(params.hash));
      } catch (e) {
        setError(e);
      }

      setLoading(false);
    };

    load();
  }, [params.hash, dispatch]);

  if (loading) return <Loading />;

  if (error) {
    return (
      <EmptyState
        uiLeftColumn
        full
        title={t('We couldn\'t find any email confirmation for this link.')}
        subtitle={t('Are you sure you got a valid link that hasn\'t been expired?')}
      >
        <Button to="/">{t('Go to home')}</Button>
      </EmptyState>
    );
  }

  return (
    <EmptyState
      uiLeftColumn
      full
      title={t('Your email has been confirmed')}
      subtitle={t('Your user account is now fully active. You can start using every feature in the site.')}
    >
      <Button to="/">{t('Go to home')}</Button>
    </EmptyState>
  );
};

Confirm.propTypes = {
};

Confirm.defaultProps = {
};

export default Confirm;
