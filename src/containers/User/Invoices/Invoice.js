import styled from 'styled-components';
import { useParams } from 'react-router-dom';
import { useSelector, shallowEqual } from 'react-redux';

import { useTranslation, useTitle, useOpenClose } from 'hooks';
import * as invoiceSelectors from 'state/invoices/selectors';

import Sidebar from 'containers/Sidebar';
import Layout from 'components/Layout';
import { FlexWrapper, FlexInnerWrapper, FlexContainer } from 'components/FlexWrapper';
import PageTitle from 'components/PageTitle';
import Button from 'components/Button';
import Check from 'components/Icons/Check';

import CouponModal from './CouponModal';
import locales from './i18n';

const Wrapper = styled.div`
  background-color: ${props => props.theme.colors.mainLight};
  padding: 16px;
  border-radius: 8px;

  section {
    border-bottom: 1px solid #ded9d9;
    margin-bottom: 24px;

    .title {
      font-size: 18px;
      font-weight: bold;
      display: flex;
      justify-content: space-between;
    }

    table {
      width: 100%;

      tr > td:last-child {
        text-align: right;
      }
    }
  }

  .actions {
    display: flex;
    justify-content: space-between;
    width: 100%;
    margin-top: 48px;
  }
`;
Wrapper.displayName = 'Wrapper';

const Payed = styled.div`
  color: ${props => props.theme.colors.success};
  font-size: 20px;
  text-align: center;
  margin-bottom: 24px;

  svg {
    width: 128px;
    height: 128px;
    border: 8px solid ${props => props.theme.colors.success};
    border-radius: 100%;
    margin-bottom: 8px;

    path {
      fill: ${props => props.theme.colors.success};
    }
  }
`;
Payed.displayName = 'Payed';

const Invoice = () => {
  const { t } = useTranslation(locales);
  useTitle(t('Invoice'));
  const { invoiceId } = useParams();

  const items = useSelector(state => invoiceSelectors.getItems(state, invoiceId), shallowEqual);
  const subtotal = useSelector(state => invoiceSelectors.getSubtotal(state, invoiceId));
  const total = useSelector(state => invoiceSelectors.getTotal(state, invoiceId));
  const coupon = useSelector(state => invoiceSelectors.getCoupon(state, invoiceId));
  const paylink = useSelector(
    state => invoiceSelectors.selectPreferenceInitPoint(state, invoiceId),
  );
  const isPayed = useSelector(state => invoiceSelectors.isPayed(state, invoiceId));

  const [isCouponModalOpen, openCouponModal, closeCouponModal] = useOpenClose();

  return (
    <Layout columns={2} feed leftColumnOpen={false} rightColumnOpen={false}>
      <FlexWrapper canOverflow>
        <FlexInnerWrapper>
          <PageTitle>{t('Invoice #{{id}}', { id: invoiceId })}</PageTitle>
          <FlexContainer framed>
            <div>
              {isPayed && (
                <Payed>
                  <Check />
                  <div>{t('This invoice is payed!')}</div>
                </Payed>
              )}
              <Wrapper>
                <section>
                  <div className="title">{t('Detail')}</div>
                  <table>
                    <tbody>
                      {items.map(item => (
                        <tr key={`item-${invoiceId}-${item.name}`}>
                          <td>{item.name}</td>
                          <td>{`${item.total} ARS`}</td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </section>

                <section>
                  <div className="title">
                    <span>{t('Sub total')}</span>
                    <span className="total">{`${subtotal} ARS`}</span>
                  </div>
                </section>

                {coupon && (
                  <section>
                    <div className="title">
                      <span>{t('Coupon')}</span>
                      <span>{coupon}</span>
                    </div>
                  </section>
                )}

                <section>
                  <div className="title">
                    <span>{t('Total')}</span>
                    <span className="total">{`${total} ARS`}</span>
                  </div>
                </section>

                <div className="actions">
                  {!coupon
                    ? <Button className="empty" onClick={openCouponModal}>{t('Enter coupon code')}</Button>
                    : <div />
                  }
                  {!isPayed && <Button to={paylink}>{t('Pay')}</Button>}
                </div>

                {/* Modals */}
                {isCouponModalOpen && (
                  <CouponModal eventId={invoiceId} close={closeCouponModal} />
                )}
              </Wrapper>
            </div>
          </FlexContainer>
        </FlexInnerWrapper>
      </FlexWrapper>

      <Sidebar />
    </Layout>
  );
};

Invoice.propTypes = {
};

Invoice.defaultProps = {
};

export default Invoice;
