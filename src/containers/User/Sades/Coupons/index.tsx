import { VStack } from '@chakra-ui/react';
import { useSelector, shallowEqual } from 'react-redux';

import * as bankSelectors from 'state/bank/selectors';


import Coupon from './Coupon';

interface Props {
}

const Coupons: React.FC<Props> = () => {
  const coupons = useSelector(bankSelectors.selectCoupons, shallowEqual);

  return (
    <VStack mt={4}>
      {coupons.map((coupon: any) => (
        <Coupon key={`coupon-${coupon.code}`} coupon={coupon} />
      ))}
    </VStack>
  );
};

export default Coupons;
