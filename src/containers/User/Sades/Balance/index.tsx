import {
  Stat,
  StatLabel,
  StatNumber,
  Button,
  HStack,
} from '@chakra-ui/react';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as authSelectors from 'state/auth/selectors';

import locales from '../i18n';

interface Props {
}

const Balance: React.FC<Props> = () => {
  const { t } = useTranslation(locales);
  const balance = useSelector(authSelectors.getSades);

  let color = 'black';
  if (balance < 15) color = 'yellow.500';
  if (balance < 5) color = 'red';

  return (
    <HStack justifyContent="space-between" alignItems="flex-end">
      <Stat>
        <StatLabel>{t('Balance')}</StatLabel>
        <StatNumber fontSize="3xl" color={color}>{`§${balance.toFixed(2)}`}</StatNumber>
      </Stat>

      <Button as={Link} variant="primary" to="/user/sades/buy">{t('Buy more')}</Button>
    </HStack>
  );
};

export default Balance;
