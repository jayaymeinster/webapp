import { memo } from 'react';
import styled from 'styled-components';

import BigLoading from 'components/Loading/BigLoading';

import logo from './logo.png';
import { DEFAULT_TITLE } from '../../constants';

const LoadingPageWrapper = styled.div`
  height: 100vh;
  display: flex;
  align-items: center;
`;

const Wrapper = styled.div`
  position: relative;
  margin: 0 auto;
  width: 64px;
  height: 64px;
`;

const LoadingWrapper = styled.div`
  position: absolute;
`;

const Logo = styled.img`
  position: absolute;
  z-index: -1;
  width: 32px;
  border-radius: 100%;
  opacity: 0.7;
  top: 16px;
  left: 16px;
`;

const LoadingPage = memo(() => (
  <LoadingPageWrapper>
    <Wrapper>
      <LoadingWrapper>
        <BigLoading />
      </LoadingWrapper>
      <Logo src={logo} alt={DEFAULT_TITLE} />
    </Wrapper>
  </LoadingPageWrapper>
));

export default LoadingPage;
