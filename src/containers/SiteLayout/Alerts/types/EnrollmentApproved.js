import PropTypes from 'prop-types';

import { useTranslation } from 'hooks';

import AlertContainer from '../AlertContainer';
import locales from '../i18n';
import { SITE_URL } from '../../../../constants';

const EnrollmentApproved = ({
  url,
  eventName,
  read,
}) => {
  const { t } = useTranslation(locales);

  const path = url.replace(SITE_URL, '');

  return (
    <AlertContainer to={path} read={read}>
      {t('You\'ve been approved to RSVP to')}
      {' '}
      <strong>{eventName}</strong>
    </AlertContainer>
  );
};

EnrollmentApproved.propTypes = {
  read: PropTypes.bool.isRequired,
  url: PropTypes.string.isRequired,
  eventName: PropTypes.string.isRequired,
};

export default EnrollmentApproved;
