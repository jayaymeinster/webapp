import PropTypes from 'prop-types';

import { useTranslation } from 'hooks';

import AlertContainer from '../AlertContainer';
import locales from '../i18n';

const SadesAssignment = ({ read }) => {
  const { t } = useTranslation(locales);

  return (
    <AlertContainer to="/bank" read={read}>
      {t('You have received the SADEs weekly assignment')}
    </AlertContainer>
  );
};

SadesAssignment.propTypes = {
  read: PropTypes.bool.isRequired,
};

export default SadesAssignment;
