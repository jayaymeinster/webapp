import PropTypes from 'prop-types';

const NewText = ({ children }) => (
  <div>{children}</div>
);

NewText.propTypes = {
  children: PropTypes.node.isRequired,
};

export default NewText;
