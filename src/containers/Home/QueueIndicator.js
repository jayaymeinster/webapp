import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as feedSelectors from 'state/feed/selectors';

import Button from 'components/Button';
import locales from './i18n';

const QueueButtonContainer = styled.div`
  position: fixed;
  padding: 16px 0;
  z-index: 100;
  width: 700px;
  top: 64px;
  backdrop-filter: saturate(180%) blur(5px);

  @media(max-width: 767px) {
    width: 100%;
  }
`;
QueueButtonContainer.displayName = 'QueueButtonContainer';

const QueueButton = styled(Button).attrs({
})`
  font-weight: 500;
  text-transform: none;
  margin-left: calc(50% - 120px);
  width: 240px;
  text-align: center;

  @media(max-width: 768px) {
    margin-left: calc((100vw / 2) - (240px / 2));
  }

  div {
    margin: 0 auto;
  }
`;
QueueButton.displayName = 'QueueButton';

const QueueIndicator = ({ onFlush }) => {
  const { t } = useTranslation(locales);

  const queueCount = useSelector(feedSelectors.getFeedQueueCount());

  if (!queueCount) return null;

  return (
    <QueueButtonContainer>
      <QueueButton onClick={onFlush}>
        {t('{{count}} new publications', { count: queueCount, context: (queueCount === 1 ? 'SINGULAR' : 'PLURAL') })}
      </QueueButton>
    </QueueButtonContainer>
  );
};

QueueIndicator.propTypes = {
  onFlush: PropTypes.func.isRequired,
};

QueueIndicator.defaultProps = {
};

export default QueueIndicator;
