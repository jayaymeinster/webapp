import { useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as eventSelectors from 'state/events/selectors';
import * as eventActions from 'state/events/actions';
import * as appActions from 'state/app/actions';

import Toggle from 'components/Toggle';
import Input from 'components/Forms/Input';
import Button from 'components/Button';

import locales from '../i18n';

const Wrapper = styled.div`
  text-align: left;

  > div {
    display: flex;
    margin-bottom: 32px;

    input {
      line-height: 42px;
    }
  }
`;
Wrapper.displayName = 'Wrapper';

const Details = ({ eventId }) => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();

  const rsvpLimit = useSelector(
    state => eventSelectors.selectRsvpLimit(state, eventId),
  );
  const [saving, setSaving] = useState(false);
  const [limit, setLimit] = useState(rsvpLimit);

  const togglersvpLimit = useCallback((e) => {
    setLimit(e.target.checked ? 100 : null);
  }, []);

  const changersvpLimit = useCallback((e) => {
    setLimit(e.target.value);
  }, []);

  const save = useCallback(async () => {
    try {
      setSaving(true);
      await dispatch(eventActions.update(eventId, { rsvpLimit: limit }));
      dispatch(appActions.addToast(t('Event updated')));
    } catch (error) {
      dispatch(appActions.addError(error));
    }

    setSaving(false);
  }, [dispatch, t, eventId, limit]);

  return (
    <Wrapper>
      <div>
        <Toggle label={t('RSVPs max')} position="left" active={limit !== null} onChange={togglersvpLimit} />
        <Input type="number" value={limit === null ? 100 : limit} disabled={limit === null} onChange={changersvpLimit} />
      </div>

      <Button onClick={save} loading={saving}>{t('global:Save')}</Button>
    </Wrapper>
  );
};

Details.propTypes = {
  eventId: PropTypes.string.isRequired,
};

Details.defaultProps = {
};

export default Details;
