import styled from 'styled-components';
import { useParams } from 'react-router-dom';

import Flyer from './Flyer';
import Details from './Details';

const Wrapper = styled.div`
  text-align: center;
`;
Wrapper.displayName = 'Wrapper';

const Edit = () => {
  const { eventId } = useParams();

  return (
    <Wrapper>
      <Flyer eventId={eventId} />

      <Details eventId={eventId} />
    </Wrapper>
  );
};

Edit.propTypes = {
};

Edit.defaultProps = {
};

export default Edit;
