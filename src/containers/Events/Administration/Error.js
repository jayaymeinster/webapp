import PropTypes from 'prop-types';
import styled from 'styled-components';

import ErrorState from 'components/ErrorState';

const Wrapper = styled.div`
  display: flex;
  height: calc(100vh - 64px);
  justify-content: center;
  align-items: center;
`;
Wrapper.displayName = 'Wrapper';

const Error = ({ message }) => (
  <Wrapper>
    <ErrorState message={message} reload={false} />
  </Wrapper>
);

Error.propTypes = {
  message: PropTypes.string.isRequired,
};

Error.defaultProps = {
};

export default Error;
