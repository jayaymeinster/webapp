import { useMemo, useCallback } from 'react';
import PropTypes from 'prop-types';
import { useSelector, shallowEqual } from 'react-redux';

import { useTranslation, useUploadMedia } from 'hooks';
import * as communitySelectors from 'state/communities/selectors';

import Composer, { EmojiPicker } from 'components/Composer';
import CommunityUploadMedia, { DropZone, UploadMediaButton } from 'components/CommunityUploadMedia';
import Select from 'components/Select';

import Wrapper from './Wrapper';
import Community from './Community';
import locales from '../i18n';

const Description = ({ composerId, communityId, setCommunityId }) => {
  const { t } = useTranslation(locales);

  const communities = useSelector(communitySelectors.selectEventableIds, shallowEqual);

  const options = useMemo(() => (
    (communities || []).map(c => ({
      value: c,
    }))
  ), [communities]);

  const {
    getInputProps,
    isDragActive,
    open,
    handlePastedFiles,
  } = useUploadMedia(composerId, '/communities/upload');

  const communityChanged = useCallback((e) => {
    setCommunityId(e.value);
  }, [setCommunityId]);

  return (
    <Wrapper>
      <h3>{t('Community')}</h3>

      <div className="communities">
        <Select
          value={options.find(c => c.value === communityId)}
          onChange={communityChanged}
          placeholder={t('Select one')}
          options={options}
          getOptionLabel={({ value }) => <Community communityId={value} />}
        />
      </div>

      <h3>{t('Description')}</h3>

      {isDragActive && (
        <DropZone>{t('global:Drop the files here...')}</DropZone>
      )}

      <div className="composer">
        <Composer
          id={composerId}
          height="250px"
          handlePastedFiles={handlePastedFiles}
          autofocus
        />
      </div>

      <div className="actions">
        <EmojiPicker id={composerId} />
        <UploadMediaButton open={open} />
      </div>

      <br />
      <CommunityUploadMedia
        id={composerId}
        open={open}
        getInputProps={getInputProps}
      />
    </Wrapper>
  );
};

Description.propTypes = {
  composerId: PropTypes.string.isRequired,
  communityId: PropTypes.string,
  setCommunityId: PropTypes.func.isRequired,
};

Description.defaultProps = {
  communityId: null,
};

export default Description;
