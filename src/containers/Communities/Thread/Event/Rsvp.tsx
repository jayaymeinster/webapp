import { FC } from 'react';

import useDisplayName from 'hooks/useDisplayName';

import UserLink from 'components/UserLink';

interface Props {
  userId: number;
}

const Rsvp: FC<Props> = ({ userId }) => {
  const displayname = useDisplayName(userId);
  return <UserLink userId={userId}>{displayname}</UserLink>;
};

export default Rsvp;
