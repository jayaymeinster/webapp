import { useState } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';

import * as appActions from 'state/app/actions';
import * as replyActions from 'state/replies/actions';

import { useTranslation } from 'hooks';

import Modal from 'components/Modal';
import Button from 'components/Button';

import locales from '../i18n';

const RemoveModal = ({ close, replyId }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation(locales);

  const [removing, setRemoving] = useState(false);

  const remove = async () => {
    try {
      setRemoving(true);

      await dispatch(replyActions.remove(replyId));
      setRemoving(false);
      close();
      dispatch(appActions.addToast(t('Reply removed')));
    } catch (error) {
      dispatch(appActions.addError(error));
      setRemoving(false);
      close();
    }
  };

  return (
    <Modal
      title={t('Remove reply')}
      onCancel={close}
      actions={[
        <Button key="remove-reply" onClick={remove} loading={removing}>{t('global:Confirm')}</Button>,
      ]}
    >
      {t('Are you sure you want to remove this reply?')}
    </Modal>
  );
};

RemoveModal.propTypes = {
  close: PropTypes.func.isRequired,
  replyId: PropTypes.string.isRequired,
};

export default RemoveModal;
