import styled from 'styled-components';

const CommunityUploadMediaContainer = styled.div`
  margin-bottom: 16px;
`;
CommunityUploadMediaContainer.displayName = 'CommunityUploadMediaContainer';

export default CommunityUploadMediaContainer;
