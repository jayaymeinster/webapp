import { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useDispatch } from 'react-redux';

import * as appActions from 'state/app/actions';
import * as channelActions from 'state/channels/actions';
import { useTranslation, useSearchUsers } from 'hooks';

import Modal from 'components/Modal';
import { UserSearchInput, UserSearchResults } from 'components/UserSearch';
import { SelectableList, SelectableListItem } from 'components/SelectableList';
import UserAvatar from 'components/UserAvatar';
import Button from 'components/Button';
import { Close } from 'components/Icons';

import locales from '../../i18n';

const SearchContainer = styled.div`
  position: relative;
  width: 100%;

  .search-container {
    background-color: white;
    box-shadow: 0px 0px 5px #9999;

    input {
      color: black;

      &::placeholder {
        color: #666;
      }
    }
  }
`;

const ResultsContainer = styled.div`
  position: relative;
  width: 100%;
`;

const AddBanModal = ({ channelId, close }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation(locales);

  const [searchProps, results, reset] = useSearchUsers();
  const [selectedUser, setSelectedUser] = useState(null);
  const [banning, setBanning] = useState(false);

  const addBan = user => setSelectedUser(user);
  const clearSelectedUser = () => {
    reset();
    setSelectedUser(null);
  };

  const ban = async () => {
    try {
      setBanning(true);
      await dispatch(channelActions.ban(channelId, selectedUser.id));
      dispatch(appActions.addToast(t('User banned')));
      close();
    } catch (error) {
      dispatch(appActions.addError(error));
      close();
    }
  };

  const modalActions = [];
  if (selectedUser) modalActions.push(<Button key="add-ban-confirm" onClick={ban} loading={banning}>{t('global:Confirm')}</Button>);

  return (
    <Modal
      title={t('Add ban')}
      onCancel={close}
      actions={modalActions}
    >
      {!selectedUser
        ? (
          <>
            <SearchContainer>
              <UserSearchInput {...searchProps} onBlurTriggered={reset} />
            </SearchContainer>
            <ResultsContainer>
              {results.length > 0 && <UserSearchResults results={results} onSelect={addBan} />}
            </ResultsContainer>
          </>
        ) : (
          <SelectableList>
            <SelectableListItem
              avatar={(<UserAvatar userId={selectedUser.id} />)}
              title={selectedUser.displayname}
              actions={[<Button key="remove-ban-confirm" onClick={clearSelectedUser} color="white" light><Close /></Button>]}
            />
          </SelectableList>
        )
      }
    </Modal>
  );
};

AddBanModal.propTypes = {
  channelId: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired,
};

export default AddBanModal;
