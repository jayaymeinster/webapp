import styled from 'styled-components';

import FireIcon from 'components/Icons/Fire';

const Wrapper = styled.div`
  margin-left: 16px;

  svg {
    width: 32px;
    height: 32px;
  }
`;
Wrapper.displayName = 'Wrapper';

const FireAction = () => (
  <Wrapper>
    <FireIcon />
  </Wrapper>
);

FireAction.propTypes = {
};

FireAction.defaultProps = {
};

export default FireAction;
