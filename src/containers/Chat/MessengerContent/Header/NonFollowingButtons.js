import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useSelector } from 'react-redux';

import * as authSelectors from 'state/auth/selectors';

const NonFollowingWrapper = styled.div`
  button {
    margin: 0 8px;
  }

  @media(max-width: 768px) {
    position: fixed;
    top: 64px;
    width: 100%;
    left: 0;
    background-color: rgba(0, 0, 0, .1);
    padding: 12px 0;
    box-sizing: border-box;
    display: flex;
    justify-content: space-around;

    button {
      margin: 0;
    }
  }
`;
NonFollowingWrapper.displayName = 'NonFollowingWrapper';

const NonFollowingButtons = ({ children, userId }) => {
  const following = useSelector(state => authSelectors.selectAmIFollowing(state, userId));

  if (following) return null;

  return <NonFollowingWrapper>{children}</NonFollowingWrapper>;
};

NonFollowingButtons.propTypes = {
  children: PropTypes.node.isRequired,
  userId: PropTypes.number.isRequired,
};

NonFollowingButtons.defaultProps = {
};

export default NonFollowingButtons;
