import styled from 'styled-components';

const OnlyYou = styled.div`
  margin-top: 16px;
  margin-right: 24px;
`;
OnlyYou.displayName = 'OnlyYou';

export default OnlyYou;
