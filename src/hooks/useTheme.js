import * as themes from 'themes';

const useTheme = () => themes.def;

export default useTheme;
