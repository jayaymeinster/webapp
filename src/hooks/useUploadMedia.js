import { useCallback } from 'react';
import { useDropzone } from 'react-dropzone';
import imageCompression from 'browser-image-compression';
import { useDispatch } from 'react-redux';

import * as appActions from 'state/app/actions';

import Api from 'state/api';

const useUploadMedia = (id, url) => {
  const dispatch = useDispatch();

  const uploadImage = useCallback(async (image) => {
    try {
      await imageCompression.getExifOrientation(image);

      const compressedFile = await imageCompression(image, {
        maxSizeMB: 1,
        maxWidthOrHeight: 1920,
      });

      dispatch(appActions.mediaUploadAdd(id, {
        file: image,
        preview: URL.createObjectURL(compressedFile),
      }));

      //

      const formData = new FormData();
      formData.append('image', compressedFile, compressedFile.name);

      const file = await Api.req({
        method: 'post',
        url,
        data: formData,
      });

      dispatch(appActions.mediaUploadComplete(id, compressedFile.name, file.data.filename));
    } catch (error) {
      dispatch(appActions.addError(error));
    }
  }, [dispatch, url, id]);

  const handlePastedFiles = useCallback((files) => {
    files.forEach((file) => {
      if (file.type.substr(0, 6) === 'image/') {
        uploadImage(file);
      }
    });
  }, [uploadImage]);

  const onDrop = useCallback((acceptedFiles) => {
    acceptedFiles.forEach((file) => {
      uploadImage(file);
    });
  }, [uploadImage]);

  const params = useDropzone({
    noClick: true,
    noKeyboard: true,
    accept: 'image/jpeg, image/png, image/gif',
    onDrop,
  });

  return {
    ...params,
    handlePastedFiles,
  };
};

export default useUploadMedia;
