import { useState, useCallback } from 'react';

const useOpenclose = (initial = false) => {
  const [isOpen, setIsOpen] = useState(initial);
  const open = (e) => {
    if (e) {
      e.stopPropagation();
      e.preventDefault();
    }
    setIsOpen(true);
  };
  const close = useCallback(() => {
    setIsOpen(false);
  }, []);

  const toggle = useCallback(() => {
    setIsOpen(currentValue => !currentValue);
  }, []);

  return [isOpen, open, close, toggle];
};

export default useOpenclose;
