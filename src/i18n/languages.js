const languages = {
  es: {
    flag: 'https://raw.githubusercontent.com/hjnilsson/country-flags/master/png100px/mx.png',
    name: 'Español (Latino)',
  },
  'es-ES': {
    flag: 'https://raw.githubusercontent.com/hjnilsson/country-flags/master/png100px/es.png',
    name: 'Español',
  },
  'es-AR': {
    flag: 'https://raw.githubusercontent.com/hjnilsson/country-flags/master/png100px/ar.png',
    name: 'Español (Argentina)',
  },
};

export default languages;
