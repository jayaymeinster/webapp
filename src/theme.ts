import { extendTheme } from '@chakra-ui/react';

const config = {
  initialColorMode: 'light',
  useSystemColorMode: false,
};

const theme = extendTheme({
  config,
  colors: {
    gray: {
      300: '#CACACA',
      400: '#A8A8A8',
      500: '#6F6666',
      800: '#323232',
    },
    brand: {
      50: '#FCE3E0',
      100: '#F8B8B2',
      // 200: '#f6a39a', // should be this one
      200: '#F5EFEF',
      300: '#F38D83',
      400: '#F1786C',
      500: '#ED4D3D',
      600: '#E22815',
      700: '#CB2413',
      800: '#B31F11',
      900: '#85170C',
    },
  },
  styles: {
    global: {
      body: {
        lineHeight: 'normal',
      },
      'img, video': {
        maxWidth: 'none',
        height: 'inherit',
      },
    },
  },
  components: {
    Button: {
      variants: {
        primary: {
          bgColor: 'black',
          color: 'white',
          _hover: {
            bgColor: 'gray.800',
          },
        },
        danger: {
          color: 'white',
          bg: 'red.500',
          _hover: {
            bg: 'red.600',
          },
          _focus: {
            bg: 'red.600',
          },
          _active: {
            bg: 'red.700',
          },
        },
      },
    },
    Modal: {
      defaultProps: {
        size: 'xl',
      },
    },
  },
});

export default theme;
