import { chakra } from '@chakra-ui/react';
import { DraggableArea as DraggableAreaComponent } from 'react-draggable-tags';

const DraggableArea = chakra(DraggableAreaComponent, {
  shouldForwardProp: (prop) => [
    'tags',
    'render',
    'onChange',
  ].includes(prop),
});

export default DraggableArea;
