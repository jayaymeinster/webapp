import PropTypes from 'prop-types';
import styled from 'styled-components';

const Main = styled.div`
  background-color: #FAFAFA;
  height: 100%;
  padding: 40px 0 0;
  flex-grow: 1;
  box-sizing: border-box;
  overflow: auto;

  @media(max-width: 768px) {
    ${props => props.hasTitle && `
      margin-top: 64px;
    `}
  }
`;

Main.propTypes = {
  hasTitle: PropTypes.bool,
};

Main.defaultProps = {
  hasTitle: false,
};

export default Main;
