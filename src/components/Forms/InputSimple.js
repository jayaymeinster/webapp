import { forwardRef } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import colors from 'utils/css/colors';

const InputWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 12px;
`;

const InputComponent = styled.input.attrs(props => ({
  type: props.type || 'text',
}))`
  font-size: 32px;
  font-weight: 600;
  margin: 0 0 ${props => (props.hasError ? '0' : '10px')};
  border: 0;
  background-color: transparent;

  ${props => props.withBorder && `
    border-bottom: 1px solid ${props.hasError ? colors.red : colors.redReactions};
  `}

  border-radius: 8px;
  transition: all 250ms ease-out;

  &:focus {
    outline: none;
  }

  @media(max-width: 767px) {
    font-size: 25px;
  }
`;

const Error = styled.div`
  color: ${colors.red};
  text-align: left;
  font-size: 12px;
  margin-bottom: 10px;
`;


const Input = forwardRef(({
  value,
  maxChars,
  error,
  onChange,
  placeholder,
  ...rest
}, ref) => {
  const hasError = (error || (maxChars && value.length > maxChars)) && value !== '';

  return (
    <InputWrapper>
      <InputComponent
        ref={ref}
        value={value}
        onChange={onChange}
        hasError={hasError}
        placeholder={placeholder}
        {...rest}
      />
      {error !== '' && <Error>{error}</Error>}
    </InputWrapper>
  );
});

Input.propTypes = {
  value: PropTypes.string.isRequired,
  maxChars: PropTypes.number,
  error: PropTypes.string,
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  withBorder: PropTypes.bool,
};

Input.defaultProps = {
  error: null,
  maxChars: null,
  onChange: null,
  withBorder: false,
  placeholder: '',
};

export default Input;
