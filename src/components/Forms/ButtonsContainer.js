import styled from 'styled-components';

const ButtonsContainer = styled.div`
  display: flex;
  justify-content: ${props => (props.center ? 'center' : 'space-between')};
  margin: 32px 0 0;
`;

export default ButtonsContainer;
