import { memo } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const AvatarWrapper = styled.div.attrs({
  className: 'avatar',
})`
  position: relative;
  width: ${props => props.size};
`;

const AvatarImg = styled.img.attrs(({ image, size }) => ({
  src: image,
  alt: 'Channel',
  width: size,
  height: size,
}))`
  border-radius: 100%;
  width: ${props => props.size};
  height: ${props => props.size};
  box-shadow: 0px 2px 7px rgba(0, 0, 0, 0.12);
  flex-shrink: 0;
  vertical-align: middle;
`;

const ChannelAvatar = memo(({
  image,
  size,
  ...props
}) => (
  <AvatarWrapper size={size} {...props}>
    <AvatarImg image={image} size={size} />
  </AvatarWrapper>
));

ChannelAvatar.propTypes = {
  image: PropTypes.string.isRequired,
  size: PropTypes.string,
};

ChannelAvatar.defaultProps = {
  size: '50px',
};

export default ChannelAvatar;
