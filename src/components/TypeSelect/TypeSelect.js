import { useCallback } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useHistory } from 'react-router-dom';

import Select from 'components/Select';

const TypeSelectComponent = styled(Select)`
  margin: 0;
  width: 200px;
  font-size: 16px;
  padding: 0 8px;
  z-index: 20;

  .icon {
    margin-right: 8px;
    margin-top: -2px;
  }
`;
TypeSelectComponent.displayName = 'TypeSelectComponent';

const TypeSelect = ({ options, value }) => {
  const history = useHistory();

  const onTypeChange = useCallback((newType) => {
    history.push(newType.to);
  }, [history]);

  return (
    <TypeSelectComponent
      onChange={onTypeChange}
      isSearchable={false}
      defaultValue={value}
      hasValue
      isClearable={false}
      options={options}
    />
  );
};

const OptionType = PropTypes.shape({
  value: PropTypes.string.isRequired,
  label: PropTypes.node.isRequired,
  to: PropTypes.string.isRequired,
});

TypeSelect.propTypes = {
  options: PropTypes.arrayOf(OptionType).isRequired,
  value: OptionType.isRequired,
};

TypeSelect.defaultProps = {
};

export default TypeSelect;
