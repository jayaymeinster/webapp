import styled from 'styled-components';

import colors from 'utils/css/colors';

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100%;

  .lds-ripple {
    display: inline-block;
    position: relative;
    width: 100px;
    height: 100px;
  }
  .lds-ripple div {
    position: absolute;
    border: 4px solid ${colors.red};
    opacity: 1;
    border-radius: 50%;
    animation: lds-ripple 1s cubic-bezier(0, 0.2, 0.8, 1) infinite;
  }
  .lds-ripple div:nth-child(2) {
    animation-delay: -0.5s;
  }
  @keyframes lds-ripple {
    0% {
      top: 20px;
      left: 20px;
      width: 16px;
      height: 16px;
      opacity: 1;
    }
    100% {
      top: -8px;
      left: -8px;
      width: 75px;
      height: 75px;
      opacity: 0;
    }
  }
`;

const BigLoading = () => (
  <Wrapper>
    <div className="lds-ripple">
      <div />
      <div />
    </div>
  </Wrapper>
);

export default BigLoading;
