import { useRef } from 'react';
import PropTypes from 'prop-types';

const FileSelector = ({ children, onChange }) => {
  const inputFile = useRef(null);
  const selectFile = () => {
    inputFile.current.click();
  };

  return (
    <>
      <children.type {...children.props} onClick={selectFile} />
      <input ref={inputFile} style={{ display: 'none' }} onChange={onChange} type="file" />
    </>
  );
};

FileSelector.propTypes = {
  children: PropTypes.node.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default FileSelector;
