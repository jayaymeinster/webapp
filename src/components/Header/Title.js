import { memo, useState } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const TitleWrapper = styled.div`
  font-size: 20px;

  ${props => props.cap && `
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;
  `}

  @media(max-width: 767px) {
    font-weight: 500;
  }

  svg {
    width: 16px;
    height: 16px;
    margin-right: 4px;
  }
`;

const Title = memo(({ children }) => {
  const [overflow, setOverflow] = useState(false);

  const toggleOverflow = () => {
    setOverflow(o => !o);
  };

  return (
    <TitleWrapper onClick={toggleOverflow} cap={!overflow}>{children}</TitleWrapper>
  );
});

Title.propTypes = {
  children: PropTypes.node,
};

Title.defaultProps = {
  children: null,
};

export default Title;
