import styled from 'styled-components';

const Container = styled.div`
  display: flex;
  padding: 12px 16px;
`;
Container.displayName = 'AddCommentContainer';

export default Container;
