import PropTypes from 'prop-types';
import { useSelector, shallowEqual } from 'react-redux';
import { HStack } from '@chakra-ui/react';

import colors from 'utils/css/colors';
import * as feedSelectors from 'state/feed/selectors';

import { Comment } from 'components/Icons';

const CommentButton = ({ publicationId, outline }) => {
  const commentIds = useSelector(
    state => feedSelectors.selectCommentIds(state, publicationId),
    shallowEqual,
  );

  return (
    <HStack spacing={0}>
      <Comment color={colors.red} outline={outline} />
      {commentIds.length > 0 && <span>{commentIds.length}</span>}
    </HStack>
  );
};

CommentButton.propTypes = {
  publicationId: PropTypes.string.isRequired,
  outline: PropTypes.bool.isRequired,
};

CommentButton.defaultProps = {
};

export default CommentButton;
