import PropTypes from 'prop-types';
import { useSelector, shallowEqual } from 'react-redux';
import { HStack, Text } from '@chakra-ui/react';

import * as feedSelectors from 'state/feed/selectors';

import PollBox from 'components/Icons/PollBox';

const Votes = ({ publicationId }) => {
  const poll = useSelector(
    state => feedSelectors.publications.selectPoll(state, publicationId),
    shallowEqual,
  );

  if (!poll) return null;

  return (
    <HStack spacing={0} h="full">
      <PollBox outline={!poll.showResults} fill="brand.500" boxSize={6} />
      <Text fontSize="lg" color="brand.500">{poll.voteCount}</Text>
    </HStack>
  );
};

Votes.propTypes = {
  publicationId: PropTypes.string.isRequired,
};

Votes.defaultProps = {
};

export default Votes;
