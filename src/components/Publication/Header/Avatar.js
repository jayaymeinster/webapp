import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import * as feedSelectors from 'state/feed/selectors';

import UserAvatar from 'components/UserAvatar';
import UserLink from 'components/UserLink';

import { AvatarWrapper } from '../UI';

const Avatar = ({ publicationId }) => {
  const authorId = useSelector(
    state => feedSelectors.publications.selectAuthorId(state, publicationId),
  );

  return (
    <AvatarWrapper>
      <UserLink userId={authorId}>
        <UserAvatar userId={authorId} size="36px" />
      </UserLink>
    </AvatarWrapper>
  );
};

Avatar.propTypes = {
  publicationId: PropTypes.string.isRequired,
};

Avatar.defaultProps = {
};

export default Avatar;
