import styled from 'styled-components';

const SearchWrapper = styled.div`
  width: 100%;
  max-width: 508px;
  margin: 0 auto 40px;
`;

export default SearchWrapper;
