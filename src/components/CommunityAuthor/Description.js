import styled from 'styled-components';

const Description = styled.div`
  line-height: 16px;
`;
Description.displayName = 'Description';

export default Description;
