import { useCallback } from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';

import { useTranslation } from 'hooks';

import EmptyState from 'components/EmptyState';
import Button from 'components/Button';

import locales from './i18n';

const ErrorState = ({ message, reload }) => {
  const { t } = useTranslation(locales);
  const history = useHistory();

  const onReloadClick = useCallback(() => {
    history.go(0);
  }, [history]);
  const title = message || t('An unexpected error occurred. Please try again later.');

  return (
    <EmptyState title={title} uiLeftColumn>
      {reload && <Button onClick={onReloadClick}>{t('Reload')}</Button>}
    </EmptyState>
  );
};

ErrorState.propTypes = {
  message: PropTypes.string,
  reload: PropTypes.bool,
};

ErrorState.defaultProps = {
  message: null,
  reload: true,
};

export default ErrorState;
