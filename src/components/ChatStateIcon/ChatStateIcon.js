import { memo } from 'react';
import PropTypes from 'prop-types';
import { Icon } from '@chakra-ui/react';

import { ProgressClock, Check, DoubleCheck } from 'components/Icons';

const ChatStateIcon = memo(({ state }) => {
  switch (state) {
    case 'SENT':
      return <Icon as={Check} fill="#666" boxSize={4} />;
    case 'RECEIVED':
      return <Icon as={DoubleCheck} fill="#666" boxSize={4} />;
    case 'READ':
      return <Icon as={DoubleCheck} fill="#4FC3F7" boxSize={4} />;
    case 'SENDING':
    default:
      return <Icon as={ProgressClock} fill="#666" boxSize={4} />;
  }
});

ChatStateIcon.propTypes = {
  state: PropTypes.oneOf(['SENT', 'RECEIVED', 'READ', 'SENDING']).isRequired,
};

ChatStateIcon.defaultProps = {
};

export default ChatStateIcon;
