import styled from 'styled-components';

const ActionButton = styled.div`
  svg {
    width: 18px;
    height: 18px;
    margin-right: 6px;
  }

  margin-left: 8px;
  cursor: ${props => props.onClick && 'pointer'};

  &:hover {
    ${props => props.animate && `
      svg {
        animation: spankIt 500ms ease-out infinite;
      }
    `}
  }

  @keyframes spankIt {
    0%, 100% {
      transform: skew(0deg);
    }
    50% {
      transform: skew(15deg);
    }
  }
`;

export default ActionButton;
