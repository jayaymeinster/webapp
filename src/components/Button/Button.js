import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

import colors from 'utils/css/colors';
import useOpenClose from 'hooks/useOpenClose';

import OptionsCaret from './OptionsCaret';
import MoreOptions from './MoreOptions';

const TextOnHover = styled.span`
  display: none;
`;
const MainContent = styled.div`
  ${props => !props.isLink && `
    word-break: break-word;
    overflow: hidden;
    text-overflow: ellipsis;
    display: flex;
  `}

  > div {
    margin: 0 auto;
  }
`;

const Loading = styled.div`
  display: inline-block;
  width: 40px !important;
  height: 20px;

  &:after {
    content: " ";
    display: block;
    width: 20px;
    height: 20px;
    margin: 1px;
    border-radius: 50%;
    border: 3px solid #fff;
    border-color: #fff transparent #fff transparent;
    animation: lds-dual-ring 1.2s linear infinite;
  }

  @keyframes lds-dual-ring {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }
`;

export const ButtonComponent = styled.button.attrs(props => ({
  disabled: props.loading || props.disabled,
  type: 'button',
  children: props.content ? props.content : props.children,
  className: props.className,
  loading: props.loading ? 'true' : undefined,
}))`
  color: ${props => (props.loading ? '#aaa' : props.fontColor)};
  font-family: 'Roboto', sans-serif;
  font-weight: bold;
  font-size: 16px;
  line-height: 28px;
  white-space: nowrap;
  text-transform: uppercase;
  text-align: center;
  height: 35px;
  width: ${props => (props.full ? '100%' : 'auto')};

  ${props => !props.light && `
    background-color: ${props.disabled ? 'grey' : props.color};
    padding: 4px 24px;
    box-shadow: 0px 2px 7px rgba(0, 0, 0, 0.12);
    border-radius: 40px;
  `}
  ${props => props.light && `
    background-color: transparent;
  `}
  ${props => props.groupOptions && `
    padding-right: 4px;
  `}

  ${props => props.size === 'big' && `
    font-size: 24px;
    height: 60px;
    max-width: 100%;

    @media(max-width: 767px) {
      font-size: 20px;
      height: 52px;
    }
  `}

  ${props => props.hasIcon && !props.loading && `
    position: relative;
    padding-left: 40px;
  `}

  ${props => props.hasMoreOptions && `
    position: relative;
    padding-right: 52px;
  `}

  border: 0;
  user-select: none;
  transition: all 250ms ease-out;
  cursor: pointer;
  text-decoration: none;

  ${props => props.to && `
    display: inline-block;
  `}

  &:focus {
    outline: none;
  }

  &:hover {
    ${props => !props.light && `
      box-shadow: 0px 2px 7px rgba(0, 0, 0, 0.20);
    `}

    ${TextOnHover} {
      display: ${({ textonhover }) => (textonhover ? 'flex' : 'none')};
    }
    ${MainContent} {
      display: ${({ textonhover }) => (textonhover ? 'none' : 'flex')};
    }
  }

  &.border {
    color: ${props => props.color};
    background-color: white;
    border: 1px solid ${props => props.color};

    &:hover {
      color: white;
      background-color: ${props => props.color};
    }
  }

  &.empty {
    color: ${props => (props.fontColor === 'white' ? colors.grey : props.fontColor)};
    background-color: transparent;
    box-shadow: none !important;
    padding: 0;

    ${props => props.loading && `
      svg {
        display: none;
      }
    `}

    ${Loading} {
      &:after {
        border: 3px solid #999;
        border-color: #999 transparent #999 transparent;
      }
    }

    &:hover {
      color: ${colors.red};
      transform: none;
    }
  }

  &.danger {
    color: ${colors.red};
  }

  &.small {
    font-size: 14px;
  }

  &.round {
    line-height: 23px;
    padding: 0;
    width: 40px;

    svg {
      width: 24px;
      height: 24px;
    }
  }

  &:focus {
    outline: none;
  }

  svg {
    width: 32px;
    height: 32px;
  }

  .icon {
    position: absolute;
    left: 12px;
    top: 6px;

    svg {
      width: 20px;
      height: 20px;
    }
  }
`;

const Button = ({
  to,
  loading,
  children,
  content,
  textOnHover,
  groupOptionChange,
  onMoreOptions,
  icon,
  ...props
}) => {
  const { disabled, groupOptions, color } = props;
  const [menuOpen, openMenu, closeMenu] = useOpenClose();

  const hasMoreOptions = !!onMoreOptions || !!groupOptions;
  const activeIcon = groupOptions ? groupOptions.find(o => o.selected).icon : icon;

  const Content = () => (
    <>
      <span className="icon">{!loading && activeIcon}</span>
      <MainContent isLink={!!to}>
        {loading && <Loading />}
        <div>{(content || children)}</div>
        {hasMoreOptions && (
          <MoreOptions onClick={onMoreOptions || openMenu} disabled={disabled} negative={color === 'white'} />
        )}
        {menuOpen && (
          <OptionsCaret
            options={groupOptions}
            onChange={groupOptionChange}
            close={closeMenu}
          />
        )}
      </MainContent>
      <TextOnHover>{textOnHover}</TextOnHover>
    </>
  );

  if (!to) {
    return (
      <ButtonComponent
        loading={loading}
        textonhover={textOnHover}
        hasIcon={!!activeIcon}
        hasMoreOptions={hasMoreOptions}
        {...props}
      >
        <Content />
      </ButtonComponent>
    );
  }

  if (to.startsWith('http')) {
    return (
      <a href={to}>
        <ButtonComponent
          to={to}
          textonhover={textOnHover}
          hasIcon={!!activeIcon}
          hasMoreOptions={hasMoreOptions}
          {...props}
        >
          <Content />
        </ButtonComponent>
      </a>
    );
  }

  return (
    <Link to={to}>
      <ButtonComponent
        to={to}
        textonhover={textOnHover}
        hasIcon={!!activeIcon}
        hasMoreOptions={hasMoreOptions}
        {...props}
      >
        <Content />
      </ButtonComponent>
    </Link>
  );
};

Button.propTypes = {
  to: PropTypes.string,
  onClick: PropTypes.func,
  color: PropTypes.string,
  loading: PropTypes.bool,
  disabled: PropTypes.bool,
  className: PropTypes.string,
  textOnHover: PropTypes.string,
  children: PropTypes.node,
  content: PropTypes.node,
  light: PropTypes.bool,
  fontColor: PropTypes.string,
  size: PropTypes.oneOf(['normal', 'big']),
  groupOptions: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired,
      icon: PropTypes.node,
      selected: PropTypes.bool,
    }),
  ),
  groupOptionChange: PropTypes.func,
  onMoreOptions: PropTypes.func,
  icon: PropTypes.node,
};

Button.defaultProps = {
  to: null,
  onClick: null,
  color: colors.red,
  loading: false,
  disabled: false,
  className: '',
  textOnHover: null,
  children: null,
  content: null,
  light: null,
  fontColor: 'white',
  size: 'normal',
  groupOptions: null,
  groupOptionChange: null,
  onMoreOptions: null,
  icon: null,
};

export default Button;
